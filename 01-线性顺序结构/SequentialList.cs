﻿namespace _01_线性顺序结构
{
    internal class SequentialList<T> : ILinearList<T>
    {
        private T[] data;
        private int length;
        private int maxSize;

        public int MaxSize
        {
            get { return maxSize; }
            set { maxSize = value; }
        }

        public SequentialList(int size)
        {
            data = new T[size];
            maxSize = size;
            length = 0;
        }

        public int Length => length;

        public T this[int index] 
        { 
            get 
            {
                if (index > length - 1)
                {
                    throw new Exception("Index out of range");
                }
                return data[index];
            }
            set
            {
                if (index > length - 1)
                {
                    throw new Exception("Index out of range");
                }
                data[index] = value;
            }
        }

        public void Append(T item)
        {
            if (IsFull())
            {
                throw new Exception("List is full");
            }
            length++;
            data[length - 1] = item;
        }

        public bool IsFull()
        {
            return length >= maxSize;
        }

        public void Clear()
        {
            length = 0;
        }

        public void Delete(int index)
        {
            if (IsEmpty())
            {
                throw new Exception("List is empty");
            }

            if (index < 0 || index > length - 1) 
            {
                throw new Exception("Index out of range");
            }

            length--;
            for (int i = index; i < length; i++) 
            {
                data[i] = data[i + 1];
            }
        }

        public T GetItem(int index)
        {
            if (IsEmpty())
            {
                throw new Exception("List is empty");
            }

            if (index < 0 || index > length - 1)
            {
                throw new Exception("Index out of range");
            }

            return data[index];
        }

        public void Insert(T item, int index)
        {
            if (IsFull())
            {
                throw new Exception("List is full");
            }

            if (index < 0 || index > length - 1)
            {
                throw new Exception("Index out of range");
            }
            length++;
            for (int i = length-1; i > index; i--)
            {
                data[i] = data[i - 1];
            }  
            data[index] = item;
        }

        public bool IsEmpty()
        {
            return length == 0;
        }

        public int LocateItem(T value)
        {
            if (value == null)
            {
                return -1;
            }

            for (int i = 0; i < data.Length; i++)
            {
                if (value.Equals(data[i]))
                {
                    return i;
                }
            }

            return -1;
        }
    }
}
