﻿using _01_线性顺序结构;

//初始化线性表，开辟空间
ILinearList<int> list = new SequentialList<int>(10);

//往线性表尾添加数据,超过定义长度会抛异常
for (int i = 8; i < 18; i++)
{
    list.Append(i);
}
string s = "线性表元素：";

for (int i = 0; i < list.Length; i++)
{
    s+= " "+list[i];
}
Console.WriteLine(s);

//查找线性表数据
int r = list.GetItem(5);
Console.WriteLine($"线性表索引为5的元素：{r}");

s = "删除索引为5的元素后：";
//删除数据
list.Delete(5);
for (int i = 0; i < list.Length; i++)
{
    s += " " + list[i];
}
Console.WriteLine(s);

s = "在索引为5的元素位置插入数据后：";
//插入数据
list.Insert(77, 5);
for (int i = 0; i < list.Length; i++)
{
    s += " " + list[i];
}
Console.WriteLine(s);
