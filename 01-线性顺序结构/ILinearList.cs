﻿namespace _01_线性顺序结构
{
    /// <summary>
    /// 插入和删除操作时间复杂度O(n)
    /// 查找时间复杂度O(1)
    /// 需要连续的空间进行数据存储，容易产生空间碎片
    /// 数组和List属于常用的线性表顺序存储结构
    /// </summary>
    /// <typeparam name="T"></typeparam>
    internal interface ILinearList<T>
    {
        /// <summary>
        /// 索引器
        /// </summary>
        /// <param name="index">索引</param>
        /// <returns></returns>
        T this[int index] { get; set; }

        /// <summary>
        /// 数组长度
        /// </summary>
        int Length { get; }

        /// <summary>
        /// 数组是否为空
        /// </summary>
        /// <returns></returns>
        bool IsEmpty();

        /// <summary>
        ///清空表 
        /// </summary>
        void Clear();

        /// <summary>
        ///通过索引获取数据元素 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        T GetItem(int index);

        /// <summary>
        /// 返回数据元素的索引
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        int LocateItem(T t);

        /// <summary>
        /// 将数据元素插入到指定位置
        /// </summary>
        /// <param name="item"></param>
        /// <param name="index"></param>
        void Insert(T item, int index);

        /// <summary>
        /// 在数组末尾添加元素
        /// </summary>
        /// <param name="item"></param>
        void Append(T item);

        /// <summary>
        /// 删除指定索引的元素
        /// </summary>
        /// <param name="index"></param>
        void Delete(int index);
    }
}
