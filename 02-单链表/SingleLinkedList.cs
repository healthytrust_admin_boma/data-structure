﻿namespace _02_单链表
{
    internal class SingleLinkedList<T> : ISingleLinkedList<T>
    {
        private Node<T>? head;

        public Node<T>? Head
        {
            get { return head; }
            set { head = value; }
        }

        public void Append(T item)
        {
            Node<T> node = new Node<T>(item);
            if (head == null)
            {
                head = node;
                return;
            }
            Node<T>? buffer = head;

            while(buffer?.Next != null)
            {
                buffer = buffer?.Next;
            }

            buffer.Next = node;
        }

        public void Clear()
        {
            head = null;
        }

        public void Delete(int i)
        {
            if (head == null || this.GetLength() < i || i <= 0)
            {
                return;
            }

            if (i == 1)
            {
                head = head.Next;
                return;
            }

            int count = 1;
            Node<T> buffer = head;
            while (buffer.Next != null)
            {
                if (count == i - 1)
                {
                    buffer.Next = buffer.Next?.Next;
                    break;
                }
                count++;
                buffer = buffer.Next;
            }
        }

        public T GetElem(int i)
        {
            if (i <= 0 || i > GetLength())
            {
                return default(T);
            }
            Node<T>? buffer = head;
            while (true) 
            {
                i--;
                if (i <= 0)
                {
                    return buffer.Data;
                }
                buffer = buffer?.Next;
            }
            return default(T);
        }

        public int GetLength()
        {
            int count = 0;
            if (head == null)
            {
                return count;
            }
            count = 1;
            Node<T> buffer = head;
            while(buffer.Next != null)
            {
                buffer = buffer.Next;
                count++;
            }
            return count;
        }

        public void Insert(T item, int i)
        {
            if (i > GetLength() + 1 || i <= 0) 
            {
                return;
            }
            Node<T>? newNode = new Node<T>(item);

            if (i == 1)
            {
                newNode.Next = head;
                head = newNode;
                return;
            }
            Node<T>? buffer = head;
            Node<T>? buffer2;
            while(true)
            {
                i--;
                if (i <= 1)
                {
                    buffer2 = buffer.Next;
                    buffer.Next = newNode;
                    newNode.Next = buffer2;
                    break;
                }
                buffer = buffer.Next;
            }
        }

        public bool IsEmpty()
        {
            return head == null;
        }
    }
}
