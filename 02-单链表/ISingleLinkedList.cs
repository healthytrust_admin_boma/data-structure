﻿namespace _02_单链表
{
    internal interface ISingleLinkedList<T>
    {
        /// <summary>
        /// 获取单链表的长度
        /// </summary>
        /// <returns></returns>
        int GetLength();

        /// <summary>
        /// 清空单链表
        /// </summary>
        void Clear();

        /// <summary>
        /// 单链表是否为空
        /// </summary>
        /// <returns></returns>
        bool IsEmpty();

        /// <summary>
        /// 在单链表末尾追加节点
        /// </summary>
        /// <param name="item"></param>
        void Append(T item);

        /// <summary>
        /// 在指定位置添加节点
        /// </summary>
        /// <param name="item"></param>
        /// <param name="i"></param>
        void Insert(T item, int i);

        /// <summary>
        /// 删除指定位置的节点
        /// </summary>
        /// <param name="i"></param>
        void Delete(int i);

        /// <summary>
        /// 获取指定位置的节点
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        T GetElem(int i);
    }
}
