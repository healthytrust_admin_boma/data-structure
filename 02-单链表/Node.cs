﻿namespace _02_单链表
{
    public class Node<T>
    {
        private T? data;

        /// <summary>
        /// 数据域
        /// </summary>
        public T? Data
        {
            get { return data; }
            set { data = value; }
        }

        private Node<T>? next;

        /// <summary>
        /// 指针域
        /// </summary>
        public Node<T>? Next
        {
            get { return next; }
            set { next = value; }
        }

        public Node()
        {
            data = default(T);
            next = null;
        }
        
        public Node(T data)
        {
            this.data = data;
        }

        public Node(Node<T> node)
        {
            next = node;
        }

        public Node(T data,Node<T> node)
        {
            this.data = data;
            next = node;
        }
    }
}
