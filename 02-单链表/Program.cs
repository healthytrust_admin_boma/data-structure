﻿using _02_单链表;

ISingleLinkedList<string> singleLinkedList = new SingleLinkedList<string>();

singleLinkedList.Append("张小妹");
singleLinkedList.Append("林小樱");
singleLinkedList.Append("王萌萌");
singleLinkedList.Append("刘莉莉");
singleLinkedList.Append("马甜甜");

int length = singleLinkedList.GetLength();
Console.WriteLine(length);

string s = singleLinkedList.GetElem(3);
Console.WriteLine(s);

singleLinkedList.Insert("老张", 6);
Console.ReadKey();